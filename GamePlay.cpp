#include <string>
#include <vector>
#include <list>
#include <cstddef>
template<typename L,typename P,typename C,typename G,typename R>class GameState {
    public:
        virtual bool isPositionRevealed(P pos) = 0;
        virtual C getMaximumGuesses() = 0;
        virtual C getAttemptedGuesses() = 0;
        virtual R makeGuess(G guess) = 0;
        virtual L getLetterAt(P pos) = 0;
        virtual ~GameState() {}
};
template<typename A>class BasicGame: public GameState<A,size_t,size_t,A,size_t> {
    private:
        std::vector<A> *word;
        std::vector<bool> *opened;
        size_t maxtries,tries;
    public:
        BasicGame() {}
        BasicGame(std::vector<A> w) {}
        void setWord(std::vector<A> w) {
            delete this->word;
            this->word = new std::vector<A>(w);
        }
        void initializeGame() {
            delete this->opened;
            this->opened = new std::vector<bool>;
            for(auto i=this->word->begin();i!=this->word->end();i++) this->opened->push_back(0);
            maxtries=tries=0;
        }
        bool isPositionRevealed(size_t k) {
            return (*(this->opened))[k];
        }
        size_t getMaximumGuesses() {
            return maxtries;
        }
        size_t getAttemptedGuesses() {
            return tries;
        }
        A getLetterAt(size_t p) {
            if((*(this->opened))[p]) return (*(this->word))[p];
            else return '\0';
        }
        size_t makeGuess(A g) {
            size_t r = 0;
            auto il = this->word->begin(),el=this->word->end();
            auto ib = this->opened->begin(),eb=this->opened->end();
            while(il!=el&&ib!=eb) {
                if(*il==g&&!(*ib)) {*ib=1;r++;}
                il++;ib++;
            }
            return r;
        }

};

